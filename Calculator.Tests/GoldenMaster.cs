using System;
using System.IO;
using System.Threading.Tasks;
using NFluent;
using Xunit;
using Calculator;
using VerifyXunit;

namespace Calculator.Tests;

public class GoldenMaster
{
    [Fact]
    public void SampleData_on_plus_on_numbers_txt()
    {
        using var writer = new StringWriter();
        Console.SetOut(writer);
        Console.SetError(writer);
        Program.Main(new string[] { "numbers.txt", "+"});
        var sut = writer.ToString();
        Check.That(sut).IsEqualTo(
            @$"
"
       );
    }
}
