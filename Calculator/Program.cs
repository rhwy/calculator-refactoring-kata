﻿namespace Calculator
{
    public static class Program
    {
        /*
         * This program takes a file name as argument and an operation (+ or *)
         * it parses the file
         * in this file, each line of the file should hopefully have a valid number
         * it should take each number and print the operation, along with the intermediary result
         * it should print at the end the total result of the defined operation applied to
         * all numbers found in this file (along with the text name of the operation)
         * ex:
         * 1
         * +2 (=3)
         * +3 (=6)
         * -------
         * total = 6 (addition)
		 *
		 * only if the app have a 3rd argument `-log` the app should show 
		 * detailed information about it's execution
         */
        public static void Main(string[] args)
        {
            Console.WriteLine($"[{DateTime.Now.ToString("hhmmss:ffffff")}][log] started");
            string ns = File.ReadAllText(args[0]);
            string o = args[1];
            string on = (o == "+" ? "addition" : (o == "*" ? "multiplication" : "unknown"));
            Console.WriteLine($"[{DateTime.Now.ToString("hhmmss:ffffff")}][log] applying operation {on}");
            int t = 0;
            int ln = 0;
            foreach (string s in ns.Split(Environment.NewLine))
            {
                int val = int.Parse(s);
                Console.WriteLine($"[{DateTime.Now.ToString("hhmmss:ffffff")}][log] parsed value = {val}");
                if (ln == 0)
                {
                    string l = (ln == 0 ? val.ToString() : o + val.ToString());
                    Console.WriteLine(l);
                }
                else
                {
                    
                    string l = (ln == 0 ? val.ToString() : o + val.ToString())
                                + " (= " + t.ToString() + ")";
                    Console.WriteLine(l);
                }
                switch (o)
                {
                    case "+":
                        t += val;
                        break;
                    case "*":
                        t *= val;
                        break;
                    default:
                        break;
                }

                ln++;
                Console.WriteLine($"[{DateTime.Now.ToString("hhmmss:ffffff")}][log] accumulation : {t} on line {ln}");
            }
            Console.WriteLine("--------------");
            Console.WriteLine("Total = " + t.ToString());
            Console.WriteLine($"[{DateTime.Now.ToString("hhmmss:ffffff")}][log] end of program");
        }
    }
}