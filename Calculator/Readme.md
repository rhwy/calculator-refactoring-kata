# Fix this calculator kata!

## About

This kata is about refactoring to SOLID. The domain is pretty simple but the
simple code implementing the problem **has bugs**,  **has no quality & structure** and **violates the
5 principles of SOLID** due to it's quick and dirty imperative style.

## Context of the existing app

> The objective of the app is to be able to realize a defined computation on all the numbers present in a file (one per line).

When this program was created it has the following requirements:

- Arguments and numbers file must be verified and throw an error with an appropriate message if they are not valid.
- It must take a file name as argument and an operation (`"+"` or `"*"`) : `myapp "numbers.txt" "+"`
- it should take each number and print the operation, along with the intermediary result
- it should print at the end the total result of the defined operation applied to all numbers found in this file (along with the text name of the operation)
- if the app is called with an additional option `-log` it should present a detailed log information of each step of the computation

examples of application call with expected results:

```shell
$> calculator "numbers.txt" "+"  

1
+2 (=3)
+3 (=6)
+4 (=10)
-------
total = 10 (addition)
```

```shell
$> calculator "numbers.txt" "+" -log

[112330:169804][log] started
[112330:208525][log] applying operation addition
[112330:208734][log] parsed value = 1
1
[112330:208832][log] accumulation : 1 on line 1
[112330:208852][log] parsed value = 2
+2 (= 1)
[112330:208861][log] accumulation : 3 on line 2
[112330:208866][log] parsed value = 3
+3 (= 3)
[112330:208871][log] accumulation : 6 on line 3
[112330:208876][log] parsed value = 4
+4 (= 6)
[112330:208881][log] accumulation : 10 on line 4
--------------
Total = 10 (addition)
[112330:208889][log] end of program
```

## What's my job then?

After you setup you project locally and have a working app, your mission is:
- discuss with your peers the problems you found in the code
- make a quick design of entities and interaction that could be defined for this app
- fix the bugs
- add tests
- refactor your app in order to make it's structure more readable and robust (aka cleaner)
- (and probably more tests will be needed)
- discuss the initial and final solutions


## Setup

-   In a new folder `CalculatorRefactoring` execute `dotnet new console`
-   Copy the content  of `calculator.cs`  below within the new create `Program.cs`
-   Create in the same folder a file `numbers.txt`  with the content below
-   Save the files and run `dotnet run numbers.txt "+"` to execute the app

If you have some difficulties running it locally, try it directly online at[onlinegdb.com](https://onlinegdb.com/qqm_FQbsU).


## Code

numbers.txt:
```csv
1
2
3
4
```

Calculator.cs:
```c#
using System;
using System.IO;

namespace PrintCalculator
{
    public class Program
    {
        /*
         * This program takes a file name as argument and an operation (+ or *)
         * it parses the file
         * in this file, each line of the file should hopefully have a valid number
         * it should take each number and print the operation, along with the intermediary result
         * it should print at the end the total result of the defined operation applied to
         * all numbers found in this file (along with the text name of the operation)
         * ex:
         * 1
         * +2 (=3)
         * +3 (=6)
         * -------
         * total = 6 (addition)
		 *
		 * only if the app have a 3rd argument `-log` the app should show 
		 * detailled information about it's execution
         */
        public static void Main(string[] args)
        {
            Console.WriteLine($"[{DateTime.Now.ToString("hhmmss:ffffff")}][log] started");
            string ns = File.ReadAllText(args[0]);
            string o = args[1];
            string on = (o == "+" ? "addition" : (o == "*" ? "multiplication" : "unknown"));
            Console.WriteLine($"[{DateTime.Now.ToString("hhmmss:ffffff")}][log] applying operation {on}");
            int t = 0;
            int ln = 0;
            foreach (string s in ns.Split(Environment.NewLine))
            {
                int val = int.Parse(s);
                Console.WriteLine($"[{DateTime.Now.ToString("hhmmss:ffffff")}][log] parsed value = {val}");
                if (ln == 0)
                {
                    string l = (ln == 0 ? val.ToString() : o + val.ToString());
                    Console.WriteLine(l);
                }
                else
                {
                    
                    string l = (ln == 0 ? val.ToString() : o + val.ToString())
                                + " (= " + t.ToString() + ")";
                    Console.WriteLine(l);
                    
                }

                switch (o)
                {
                    case "+":
                        t += val;
                        break;
                    case "*":
                        t *= val;
                        break;
                    default:
                        break;
                }

                ln++;
                Console.WriteLine($"[{DateTime.Now.ToString("hhmmss:ffffff")}][log] accumulation : {t} on line {ln}");
            }
            Console.WriteLine("--------------");
            Console.WriteLine("Total = " + t.ToString());
            Console.WriteLine($"[{DateTime.Now.ToString("hhmmss:ffffff")}][log] end of program");
        }
    }
}
```